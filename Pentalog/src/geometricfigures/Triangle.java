package geometricfigures;

public class Triangle extends Shape {
	private double sideA;
	private double sideB;
	private double sideC;

	public Triangle(int sideA, int sideB, int sideC) {
		this.sideA = sideA;
		this.sideB = sideB;
		this.sideC = sideC;
	}
	public Triangle(){
		
	}

	@Override
	public double calculateArea() {
		double p = calculatePerimeter() / 2;
		return Math.sqrt(p * (p - sideA) * (p - sideB) * (p - sideC));

	}

	@Override
	public double calculatePerimeter() {
		return sideA + sideB + sideC;
	}

	public boolean isTriangleValid() {
		if ((sideA + sideB) > sideC && (sideA + sideC) > sideB && (sideB + sideC) > sideA) {
			return true;
		} else {
			return false;
		}
	}
	public double getSideA() {
		return sideA;
	}
	public void setSideA(double sideA) {
		this.sideA = sideA;
	}
	public double getSideB() {
		return sideB;
	}
	public void setSideB(double sideB) {
		this.sideB = sideB;
	}
	public double getSideC() {
		return sideC;
	}
	public void setSideC(double sideC) {
		this.sideC = sideC;
	}

	
}
