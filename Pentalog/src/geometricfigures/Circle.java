package geometricfigures;

public class Circle extends Shape{
	private double radius;
	@Override
	public double calculateArea() {
		return 2 * Math.PI * radius;
	}
	@Override
	public double calculatePerimeter() {
		return radius * radius * Math.PI;
	}

	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	


}
