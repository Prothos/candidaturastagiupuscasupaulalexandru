package geometricfigures;

public class Rectangle extends Shape {
	private double length;
	private double width;

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	@Override
	public double calculateArea() {
		return length * width;
	}

	@Override
	public double calculatePerimeter() {
		return 2 * (length + width);
	}
	public boolean isValidRectangle(){
		if(width>0&&length>0){
			return true;
		}
		return false;
	}

}
