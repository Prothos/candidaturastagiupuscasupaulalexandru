package geometricfigures;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Calculate {
	private Scanner sc = new Scanner(System.in);

	public void start() {
		char shapeChoice = userShapeChoice();
		if (shapeChoice == 'c')
			choiceCircle();
		if (shapeChoice == 'r')
			choiceRectangle();
		if (shapeChoice == 't')
			choiceTriangle();
		restartChoice();
	}

	private void restartChoice() {
		char userChoice = 0;
		System.out.println("Do you want to try again? Y/N");
		while (userChoice != 'y' && userChoice != 'n') {
			try {
				userChoice = sc.next().toLowerCase().charAt(0);
			} catch (InputMismatchException e) {
				System.out.println("We only accept valid inputs here.");
			}
			if (userChoice == 'y') {
				start();
			} else if (userChoice != 'y' && userChoice != 'n') {
				System.out.println("You can only choose Y or N.");
			}
		}
	}

	private char userShapeChoice() {

		char userInput = 0;
		while (userInput != 'c' && userInput != 't' && userInput != 'r') {
			System.out.println("Press C for circle T for triangle or R for rectangle.");
			userInput = sc.next().toLowerCase().charAt(0);
		}
		return userInput;
	}

	private void choiceCircle() {
		Circle circle = new Circle();
		System.out.println("Please input the radius of your circle.");
		try {
			while (circle.getRadius() <= 0) {
				circle.setRadius(sc.nextDouble());
				if (circle.getRadius() <= 0) {
					System.out.println("Radius must be possitive");
				}
			}
			System.out.println("The Perimeter of your circle is : " + circle.calculatePerimeter());
			System.out.println("The Area of your circle is : " + circle.calculateArea());
		} catch (InputMismatchException e) {
			System.out.println("You should only enter numbers.");
			sc.nextLine();
		}

	}

	private void choiceTriangle() {
		Triangle triangle = new Triangle();
		System.out.println("Please input 3 numbers they will be the three sides of your triangle");
		try {
			while (!triangle.isTriangleValid()) {
				triangle.setSideA(sc.nextDouble());
				triangle.setSideB(sc.nextDouble());
				triangle.setSideC(sc.nextDouble());
				if (!triangle.isTriangleValid()) {
					System.out.println("The numbers you chose do not create a valid triangle.Try again.");
				}
			}
			System.out.println("The Perimeter of your triangle is : " + triangle.calculatePerimeter());
			System.out.println("The Area of your triangle is : " + triangle.calculateArea());
		} catch (InputMismatchException e) {
			System.out.println("You should only enter numbers.");
			sc.nextLine();
		}

	}

	private void choiceRectangle() {
		Rectangle rectangle = new Rectangle();
		System.out.println("Please input 2 numbers they will be the width and height of your rectangle");
		try {
			while (!rectangle.isValidRectangle()) {
				rectangle.setWidth(sc.nextDouble());
				rectangle.setLength(sc.nextDouble());
				if (!rectangle.isValidRectangle()) {
					System.out.println("The numbers must be possitive");
				}
			}
			System.out.println("The Perimeter of your rectangle is : " + rectangle.calculatePerimeter());
			System.out.println("The Area of your rectangle is : " + rectangle.calculateArea());
		} catch (InputMismatchException e) {
			System.out.println("You should only enter numbers.");
			sc.nextLine();
		}

	}
}
